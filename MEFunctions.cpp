/*
********************************************************************
(c) MSU Video Group 2003-2010, http://compression.ru/video/
This source code is property of MSU Graphics and Media Lab

This code can be distributed and used only with WRITTEN PERMISSION.
********************************************************************
*/

#include "types.h"
#include <assert.h>
#include <vector>
#include <algorithm>
#include <omp.h>

#define GetError GetErrorSAD_16x16
#define GetErr(spl, cur, prev, wext) (((spl) == 1) ? GetErrorSAD_8x8((cur), (prev), (wext)) : GetError((cur), (prev), (wext)))
#define SPLIT_TRESH (2250000 / (quality * quality))

MV *prev_vectors;
const int step[8] = { 0xd0, 0xe0, 0x68, 0x94, 0x29, 0x16, 0x07, 0x0b }; // this array shows which vector we have already tried, using bitmasks
const int dx[8] = { -1, 0, 1, -1, 1, -1, 0, 1 };
const int dy[8] = { -1, -1, -1, 0, 0, 1, 1, 1 };

const int diam_dy[8] = { 0, -1, -2, 1, -1, 2, 1, 0 };
const int diam_dx[8] = { -2, -1, 0, -1, 1, 0, 1, 2 };
const int diaml_dy[4] = { 0, -1, 0, 1 };
const int diaml_dx[4] = { -1, 0, 1, 0 };

void copy(MV *prev, MV *cur, int num_blocks_vert, int num_blocks_hor)
{
    for (int i = 0; i < num_blocks_vert; i++)
        for (int j = 0; j < num_blocks_hor; j++) {
            prev[i * num_blocks_hor + j] = cur[i * num_blocks_hor + j];
        }
}

void inline(MV *pmv, int x, int y, Shift_Dir dir, int error)
{
    pmv->x = x;
    pmv->y = y;
    pmv->dir = dir;
    pmv->error = error;
}

void search(int wext, BYTE *cur, BYTE *prev, MV *prob_motion_vector, Shift_Dir dir, int quality, int i, int j, int nbh, int nbv, int tp, int &min_error, MV *tfv, int spl)
{
    int kp = 0, lp = 0, k = 0, l = 0, temp = 0, error = 0, prev_st = 0, kt = 0, lt = 0, prev_all = 0, num_it = 0, cnt = 0;
    bool f = true;
    std::vector <std::pair <int, int>> candidates;
    candidates.reserve(20);
    if (dir == sd_none) {
        int li_bound = (tp > 1) * nbv / 2;
        int ui_bound = nbv / (2 - (tp > 1));
        int lj_bound = (tp & 1) * nbh / 2;
        int uj_bound = nbh / (2 - (tp & 1));
        candidates.push_back(std::make_pair(prev_vectors[i * nbh + j].y, prev_vectors[i * nbh + j].x));
        for (int t = 0; t < 8; t++) {
            if (i + dy[t] >= 0 && i + dy[t] < nbv && j + dx[t] >= 0 && j + dx[t] < nbh) {
                k = prev_vectors[(i + dy[t]) * nbh + j].y;
                l = prev_vectors[i * nbh + j + dx[t]].x;
                if (k < -MAX_MOTION || k >= MAX_MOTION || l < -MAX_MOTION || l >= MAX_MOTION) // don't reach borders
                    continue;
                candidates.push_back(std::make_pair(k, l));
            }
        }
        for (int t = 0; t < 4; t++) {
            if (i + dy[t] >= li_bound && i + dy[t] < ui_bound && j + dx[t] >= lj_bound && j + dx[t] < uj_bound) {
                k = tfv[(i + dy[t]) * nbh + j].y;
                l = tfv[i * nbh + j + dx[t]].x;
                if (k < -MAX_MOTION || k >= MAX_MOTION || l < -MAX_MOTION || l >= MAX_MOTION) // don't reach borders
                    continue;
                candidates.push_back(std::make_pair(k, l));
            }
        }
        std::sort(candidates.begin(), candidates.end());
        candidates.erase(std::unique(candidates.begin(), candidates.end()), candidates.end());
        // trying to use candidates from previous frame and previous blocks of current frame
        cnt = candidates.size();
        for (int t = 0; t < cnt; t++) {
            k = candidates[t].first;
            l = candidates[t].second;
            temp = k * wext;
            error = GetErr(spl, cur, prev + temp + l, wext);
		    if(error < min_error)
		    {
			    prob_motion_vector->x = l;
			    prob_motion_vector->y = k;
			    prob_motion_vector->dir = dir;
			    prob_motion_vector->error = error;
			    min_error = error;
		    }
        }
   
        // checking the center of diamond
        error = GetErr(spl, cur, prev, wext);
	    if(error < min_error)
	    {
		    prob_motion_vector->x = 0;
		    prob_motion_vector->y = 0;
		    prob_motion_vector->dir = dir;
		    prob_motion_vector->error = error;
		    min_error = error;
            kp = 0;
            lp = 0;
	    }
        // diamon search by itself
        prev_st = 0;
        kt = prob_motion_vector->y;
        lt = prob_motion_vector->x;
        int num_it = 0;
        while (f) {
            f = false;
            prev_all = prev_st;
            kp = kt;
            lp = lt;
            ++num_it;
            if (num_it >= quality * quality / 800) {
                break;
            }
            for (int t = 0; t < 8; t++) {
                k = kp + diam_dy[t];
                l = lp + diam_dx[t];
                if (k < -MAX_MOTION || k >= MAX_MOTION || l < -MAX_MOTION || l >= MAX_MOTION) // don't reach borders
                    continue;
                if (!(prev_all & (1 << t))) {
                    temp = k * wext;
                    error = GetErr(spl, cur, prev + temp + l, wext);
			        if(error < min_error)
			        {
				        prob_motion_vector->x = l;
				        prob_motion_vector->y = k;
				        prob_motion_vector->dir = dir;
				        prob_motion_vector->error = error;
				        min_error = error;
                        kt = k;
                        lt = l;
                        prev_st = step[t];
                        f = true;
			        }
                }
            }
        }

        for (int t = 0; t < 4; t++) {
            k = kp + diaml_dy[t];
            l = lp + diaml_dx[t];
            if (k < -MAX_MOTION || k >= MAX_MOTION || l < -MAX_MOTION || l >= MAX_MOTION) // don't reach borders
                continue;
            temp = k * wext;
            error = GetErr(spl, cur, prev + temp + l, wext);
		    if(error < min_error)
		    {
			    prob_motion_vector->x = l;
			    prob_motion_vector->y = k;
			    prob_motion_vector->dir = dir;
			    prob_motion_vector->error = error;
			    min_error = error;
		    }
        }
        return;
    }
    if (min_error == 0) {
        return;
    }
    f = true;
    kt = prob_motion_vector->y;
    lt = prob_motion_vector->x;
    kp = kt;
    lp = lt;
    for (int t = 0; t < 8; t++) {
        k = kp + dy[t];
        l = lp + dx[t];
        if (k < -MAX_MOTION || k >= MAX_MOTION || l < -MAX_MOTION || l >= MAX_MOTION) // don't reach borders
            continue;
        temp = k * wext;
        error = GetErr(spl, cur, prev + temp + l, wext);
		if(error < min_error)
		{
			prob_motion_vector->x = l;
			prob_motion_vector->y = k;
			prob_motion_vector->dir = dir;
			prob_motion_vector->error = error;
			min_error = error;
		}
    }
}

void call_search (BYTE* cur_Y_frame, BYTE* prev_Y_frame, BYTE* prev_Y_frame_up, BYTE* prev_Y_frame_l, BYTE* prev_Y_frame_upl, 
                  int wext, int quality, int nbh, int nbv, int t, MV *tfv, int use_half_pixel)
{
    int spl = 0;
    int min_error = 0;
    MV prob_motion_vector;
    int first_row_offset = wext * BORDER + BORDER;
	BYTE *cur, *prev;
    for (int i = (t > 1) * nbv / 2; i < nbv / (2 - (t > 1)); i++)
	{
		for (int j = (t & 1) * nbh / 2; j < nbh / (2 - (t & 1)); j++)
		{
            memset(&prob_motion_vector, 0, sizeof(MV));
            min_error = 1e9;
			int vert_offset = ( i << 4 ) * wext + first_row_offset;
			int hor_offset = (j << 4);
			cur = cur_Y_frame + vert_offset + hor_offset;
			prev = prev_Y_frame + vert_offset + hor_offset;
            search(wext, cur, prev, &prob_motion_vector, sd_none, quality, i, j, nbh, nbv, t, min_error, tfv, spl);

			if(use_half_pixel)
			{
				prev = prev_Y_frame_up + vert_offset + hor_offset;
				search(wext, cur, prev, &prob_motion_vector, sd_up, quality, i, j, nbh, nbv, t, min_error, tfv, spl);


				prev = prev_Y_frame_l + vert_offset + hor_offset;
				search(wext, cur, prev, &prob_motion_vector, sd_l, quality, i, j, nbh, nbv, t, min_error, tfv, spl);


				prev = prev_Y_frame_upl + vert_offset + hor_offset;
				search(wext, cur, prev, &prob_motion_vector, sd_upl, quality, i, j, nbh, nbv, t, min_error, tfv, spl);

			} // halfpixel
			tfv[ i * nbh + j ] = prob_motion_vector;
        }
	}
}

void MEFunction(BYTE* cur_Y_frame, BYTE* prev_Y_frame, BYTE* prev_Y_frame_up, BYTE* prev_Y_frame_l, BYTE* prev_Y_frame_upl, int width, int height, MV *MVectors, BYTE quality, bool use_half_pixel)
{
    if (!quality)
        quality = 1;
	int wext = width + BORDER * 2;
    int num_blocks_vert = (height + 15) >> 4, num_blocks_hor = (width + 15) >> 4;
    memset(MVectors, 0, sizeof(MV) * num_blocks_vert * num_blocks_hor);
//#pragma omp parallel for
    for (int t = 0; t < 4; t++) {
          call_search(cur_Y_frame, prev_Y_frame, prev_Y_frame_up, prev_Y_frame_l, prev_Y_frame_upl, wext, quality, num_blocks_hor, num_blocks_vert, t, MVectors, use_half_pixel);
    }
    copy(prev_vectors, MVectors, num_blocks_vert, num_blocks_hor);
}

void MEStart( int width, int height, BYTE quality )
{
    int st = MAX_MOTION / 2; 
	int num_blocks_vert = (height + 15) >> 4, num_blocks_hor = (width + 15) >> 4;
    // vectors from previous frame
    prev_vectors = new MV[num_blocks_vert * num_blocks_hor];
    memset(prev_vectors, 0, sizeof(MV) * num_blocks_vert * num_blocks_hor);
}

void MEEnd(int quality)
{
    delete [](prev_vectors);
}

